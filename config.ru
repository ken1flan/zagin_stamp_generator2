require "roda"

class App < Roda
  route do |r|
    r.root do
      "Zagin Stamp Generator 2"
    end
  end
end

run App.freeze.app
